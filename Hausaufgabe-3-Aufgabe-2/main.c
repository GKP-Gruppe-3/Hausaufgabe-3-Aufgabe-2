#include "stdafx.h"

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>

struct point
{
  double x;
  double y;
};

typedef struct point point_t;

struct line
{
  point_t point1;
  point_t point2;
};

typedef struct line line_t;

struct linear_equation
{
  double slope;
  double yIntercept;
};

typedef struct linear_equation linear_equation_t;

void pause()
{
  printf("Druecken sie eine beliebige Taste...\n");
  _getch();
  system("cls");
}

// Gets a double value from console.
double consoleGetDouble(char *title)
{
  double d;
  printf(title);
  scanf_s("%lf", &d);
  return d;
}

// Gets a point value from console.
point_t consoleGetPoint(char *title)
{
  point_t point;
  printf(title);
  point.x = consoleGetDouble("x: ");
  point.y = consoleGetDouble("y: ");
  return point;
}

void putPoint(char *title, point_t *point)
{
  printf("%s:\n", title);
  printf("    x = %.2lf\n", point->x);
  printf("    y = %.2lf\n", point->y);
}

// Gets a line value from console.
line_t consoleGetLine(char *title)
{
  line_t line;
  printf(title);
  printf("\n");
  line.point1 = consoleGetPoint("Bitte den ersten Punkt eingeben...\n");
  line.point2 = consoleGetPoint("Bitte den zweiten Punkt eingeben...\n");
  return line;
}

// Den Mittelpunkt aus einer Geraden berechnen
point_t getMidpointByLine(line_t *line)
{
  // x_m = (x1 + x2) / 2
  // y_m = (y1 + y2) / 2

  point_t midpoint;
  midpoint.x = (line->point1.x + line->point2.x) / 2;
  midpoint.y = (line->point1.y + line->point2.y) / 2;
  return midpoint;
}

void putLine(char *title, line_t *line)
{
  printf("%s:\n", title);
  printf("    Punkt 1:\n");
  printf("        x = %.2lf\n", line->point1.x);
  printf("        y = %.2lf\n", line->point1.y);
  printf("    Punkt 2:\n");
  printf("        x = %.2lf\n", line->point2.x);
  printf("        y = %.2lf\n", line->point2.y);
}

// Geradengleichung aus einer Linie erstellen
linear_equation_t getLinearEquationByLine(line_t *line)
{
  // "y = (m * x) + n"
  // "x = (y - n) / m"
  // "n = y - (m * x)" y-intercept
  // "m = (y - n) / x" slope
  // "m = (y2 - y1) / (x2 - x1)" slope

  linear_equation_t linearEquation;

  linearEquation.slope = (line->point2.y - line->point1.y) / (line->point2.x - line->point1.x);
  linearEquation.yIntercept = line->point2.y - (linearEquation.slope * line->point2.x);

  return linearEquation;
}

// Lotgleichung über eine Geradengleichung und einen Lotpunkt berechnen
linear_equation_t getPerpendicularLinearEquation(linear_equation_t *linearEquation, point_t *point)
{
  // m_{Lot} = -1 / m
  // "n = y - (m * x)" y-intercept

  linear_equation_t perpendicularEquation;
  perpendicularEquation.slope = -1.0 / linearEquation->slope;
  perpendicularEquation.yIntercept = point->y - (perpendicularEquation.slope * point->x);
  return perpendicularEquation;
}

void putLinearEquation(char *title, linear_equation_t *linearEquation)
{
  printf("%s:\n", title);
  printf("    m (Anstieg)      = %.2lf\n", linearEquation->slope);
  printf("    n (Verschiebung) = %.2lf\n", linearEquation->yIntercept);
}

// Den y-Wert zu einem x-Wert und einer Geradengleichung ermitteln.
double linearEquationGetY(linear_equation_t *linearEquation, double x)
{
  // "y = (m * x) + n"
  return (linearEquation->slope * x) + linearEquation->yIntercept;
}

// Den Schnittpunkt zweier Geradengleichung ermitteln.
point_t linearEquationGetIntersection(linear_equation_t *linearEquation1, linear_equation_t *linearEquation2)
{
  // "y = (m * x) + n"
  // "(m1 * x) + n1 = (m2 * x) + n2"
  // "(m1 * x) - (m2 * x) = n2 - n1"
  // "x * (m1 - m2) = n2 - n1"
  // "x = (n2 - n1) / (m1 - m2)"
  point_t intersection;
  intersection.x =
    (linearEquation2->yIntercept - linearEquation1->yIntercept) /
    (linearEquation1->slope - linearEquation2->slope);
  intersection.y = linearEquationGetY(linearEquation1, intersection.x);
  return intersection;
}

/* Testwerte (mit Zeilenumbrüchen auf die Konsole kopieren):
Start:
1.9
8.2
3.7
4.6
1.1
e-12.34
q15.26
14.87

Start:
1
2
0
0
e1.1
e3.4
q3
1
*/
void excercise2()
{
  // 2.2 - Geradengleichung aus zwei Punkten ermitteln
  line_t line = consoleGetLine("Bitte eine Linie anhand von zwei Punkten eingeben...");
  putLine("Linie", &line);

  linear_equation_t lineEquation = getLinearEquationByLine(&line);
  putLinearEquation("Geradengleichung", &lineEquation);

  printf("\n");

  // 2.3 - Mittelpunkt der Gerade
  point_t midpoint = getMidpointByLine(&line);
  putPoint("Mittelpunkt", &midpoint);

  printf("\n");

  // 2.4 - Beliebiger Punkt auf der Geraden: x-Wert als Eingabe, y-Wert als Ausgabe
  double x, y;
  int c = 0;
  printf("Jetzt koennen sie x-Werte eingeben, fuer die jeweils der y-Wert auf der Geraden ausgeben werden soll...\n");
  for (int i = 0; ; i++)
  {
    printf("Falls sie keinen (weiteren) x-Wert eingeben wollen, druecken sie <ESC> oder 'q'...\n");
    c = _getch();
    if (c == 27 || c == (int)'q' || c == (int)'Q')
    {
      break;
    }
    x = consoleGetDouble("Bitte den x-Wert fuer den gesuchten y-Wert auf der Geraden eingeben: ");
    y = linearEquationGetY(&lineEquation, x);
    printf("Der zugehoerige y-Wert ist: %lf\n", y);
  }
  printf("\n");

  // 2.5 - Lot auf der Gerade
  // m_{Lot} = tan(-atan(m))
  point_t perpendicularPoint = consoleGetPoint("Bitte den Lotpunkt eingeben...\n");
  linear_equation_t perpendicularEquation = getPerpendicularLinearEquation(&lineEquation, &perpendicularPoint);
  putPoint("Lotpunkt", &perpendicularPoint);
  putLinearEquation("Lotgleichung", &perpendicularEquation);
  point_t perpendicularFoot = linearEquationGetIntersection(&lineEquation, &perpendicularEquation);
  putPoint("Fusspunkt des Lots/Schnittpunkt der Geraden und des Lots", &perpendicularFoot);

  printf("\n");
}

int main(void)
{
  excercise2();
  pause();

  return 0;
}
